function createAndAppend(tagname, classname, destinationId) {

	var element = document.createElement(tagname);
	element.className = classname;
	document.getElementById(destinationId).appendChild(element);
	return element;
}

function createGameField(imageFullBase, size, cellsCount) {

	document.getElementById("gamefield").innerHTML = "";

	for (var y=0;y<size.height;y++) {
		for (var x=0;x<size.width;x++) {

			createAndAppend("div","cell","gamefield").onclick = (function(image) {
				return function() {
					checkClicks(this, image, cellsCount);
				}
			})(extractRandElement(imageFullBase));
		}
		createAndAppend("div","nextline","gamefield");
	}
}

function extendAndMixArray(arr, length) {

	var result = [];
	for (var i=0;i<length;i+=2) {
		var randElem = arr[parseInt(Math.random() * arr.length)];
		result.push(randElem, randElem);
	}

	return result;
}

function extractRandElement(arr) {
	return arr.splice(parseInt(Math.random() * arr.length), 1);
}


function setCellImage(self, image) {

	if (image) {
		self.style.backgroundImage = "url(" + image + ")";
		self.style.opacity = 0.9;
	}
	else {
		self.style.backgroundImage = "none";
		self.style.opacity = 0.5;
	}
}

var clickStep = 0;
var clickedItems = [];
var openedCount = 0;
var scoreCounterID = 0;

function checkClicks(self, image, cellsCount) {

	if (self.style.backgroundImage!="none" && self.style.backgroundImage!="initial" && self.style.backgroundImage!="") return;
	setCellImage(self, image);

	if (clickStep==2) {
		if (clickedItems[0].style.backgroundImage!=clickedItems[1].style.backgroundImage) {
			setCellImage(clickedItems[0],"");
			setCellImage(clickedItems[1],"");
			openedCount-=2;
		}
		clickStep = 0;
	}
	
	openedCount++;
	clickedItems[clickStep++]=self;
	if (openedCount==cellsCount) {
		setMessage("Победа!<br> Количество набранных очков: " + nowScores + "<br>Затраченное время: " + getTimerString() + "<br>Открытых картинок: " + openedCount);
		stopGame();
	}
}

var nowScores;
var timeSpent;

function setMessage(text) {
	document.getElementById("gamefield").innerHTML = text;
}

function getTimerString() {
	return (parseInt(timeSpent / 60) + 100 + "").substr(1) + ":" + (timeSpent % 60 + 100 + "").substr(1);
}

function scoreCounter() {

	nowScores--;
	timeSpent++;
	document.getElementById("pointsBlock").innerHTML = nowScores;
	document.getElementById("timeBlock").innerHTML = getTimerString();

	if (nowScores<=0) {
		setMessage("Игра окончена. Набрано 0 очков.");
		stopGame();
	}

}

function stopGame() {
	if (scoreCounterID) clearInterval(scoreCounterID);
	nowScores = 0;
	timeSpent = 0;
	clickStep = 0;
	clickedItems = [];
	openedCount = 0;
	scoreCounterID = 0;
}

function startGame() {

	stopGame();

	var imageBase = ["https://kde.link/test/1.png","https://kde.link/test/2.png",
	"https://kde.link/test/9.png","https://kde.link/test/7.png",
	"https://kde.link/test/6.png","https://kde.link/test/3.png",
	"https://kde.link/test/4.png","https://kde.link/test/0.png",
	"https://kde.link/test/5.png","https://kde.link/test/8.png"];

	setMessage("Загрузка...");
	getJSON("https://kde.link/test/get_field_size.php", function(size) {

		var cellsCount = size.width * size.height;
		var imageFullBase = extendAndMixArray(imageBase, cellsCount);

		createGameField(imageFullBase, size, cellsCount);
		
		nowScores = cellsCount * 10;		
		timeSpent = 0;
		scoreCounterID = setInterval("scoreCounter()", 1000);
	});
}

