function getJSON(url, callback)
{
	var req = null;
	if (window.XMLHttpRequest) {
		try {
			req = new XMLHttpRequest();
		} catch (e){}
	} else if (window.ActiveXObject) {
		try {
			req = new ActiveXObject('Msxml2.XMLHTTP');
		} catch (e){
			try {
				req = new ActiveXObject('Microsoft.XMLHTTP');
			} catch (e){}
		}
	}

	if (req) {       
		req.open("GET", url, true);
		req.onreadystatechange = function() {
			
			try {
				if (req.readyState == 4) {
					if (req.status == 200) {
						callback(JSON.parse(req.responseText));
					} else {
						alert("Не удалось получить данные:\n" +
						req.statusText);
					}
				}
			}
			catch( e ) {  }
			
		}
		req.send(null);
	}
}